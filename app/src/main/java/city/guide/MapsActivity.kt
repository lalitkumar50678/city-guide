package city.guide

import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.view.View
import city.guide.apicalling.ApiCalling
import city.guide.interfaces.ResponseInterface
import city.guide.models.LatLongModel
import city.guide.network.isConnectedToNetwork
import city.guide.utils.API_URL
import city.guide.utils.BASE_URL
import city.guide.utils.v
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_maps.*
import com.google.android.gms.maps.CameraUpdate
import com.google.android.gms.maps.model.BitmapDescriptorFactory


class MapsActivity : AppCompatActivity(), OnMapReadyCallback,View.OnClickListener, ResponseInterface {

    private lateinit var mMap: GoogleMap
    private var LOG_TAG: String = this.javaClass.simpleName
    private var isPlaying: Boolean = false
    private val handler = Handler()
    private val DELAY : Long = 15000; // 15 second delay

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_maps)
        val mapFragment = supportFragmentManager
                .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
        playAndStop_FloatingActionButton.setOnClickListener(this)

    }




    var runnable = object : Runnable {
        override fun run() {
            //do something
            handler.postDelayed(this, DELAY)
            if(isConnectedToNetwork(applicationContext)){
                val apiCalling = ApiCalling()
                apiCalling.getLocation(API_URL, this@MapsActivity)

            }
        }
    }

    private fun getapiEvery15Sec(OnOff : Boolean) {
        if(OnOff) {
            handler.postDelayed(runnable, 0)
        }else {
            handler.removeCallbacks(runnable)
        }
    }

    /**
     *empty Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
    }

    override fun onClick(v: View?) {
        when(v){
            playAndStop_FloatingActionButton ->{
                v(LOG_TAG, "playAndStop_FloatingActionButton clicked isPlaying:" + isPlaying)

                if (isConnectedToNetwork(this)) {

                    val imageDrawableToShow: Int
                    if (isPlaying) {
                        imageDrawableToShow = R.drawable.ic_play_arrow_white_24dp
                        getapiEvery15Sec(false)
                    } else {
                        imageDrawableToShow = R.drawable.ic_stop_white_24dp
                        getapiEvery15Sec(true)
                    }

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        playAndStop_FloatingActionButton.setImageDrawable(resources.
                                getDrawable(imageDrawableToShow, theme))
                    } else {
                        playAndStop_FloatingActionButton.setImageDrawable(resources.getDrawable(imageDrawableToShow))
                    }

                    isPlaying = !isPlaying
                } else {
                    Snackbar.make(playAndStop_FloatingActionButton,
                            getString(R.string.PleaseConnectToTheWiFiNetworkWhichHasTheAppServerRunningOnIt), Snackbar.LENGTH_LONG).show()
                }
            }
        }
    }

    override fun onFailListener(response: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onResponseListener(response: String, apiName: String) {
        v(LOG_TAG,"response "+ response)
        if(apiName.contentEquals(API_URL)){
            val gson = Gson()
            val latLongModel = gson.fromJson<LatLongModel>(response, LatLongModel::class.java!!)
            val lat = java.lang.Double.parseDouble(latLongModel.latitude)
            val long = java.lang.Double.parseDouble(latLongModel.longitude)
            val coordinate = LatLng(lat,long)
            val location = CameraUpdateFactory.newLatLngZoom(
                    coordinate, 15f)
            val icon = BitmapDescriptorFactory.fromResource(R.mipmap.marker)
            mMap.clear()
            mMap.animateCamera(location)
            mMap.addMarker(MarkerOptions().
                    position(coordinate).
                    title(getString(R.string.MarkerInJodhpur))
                    .icon(icon)
                    .anchor(0.5f, 1f))
        }
    }

}