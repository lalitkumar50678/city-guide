package city.guide.utils

import android.util.Log

/**
 * Debugging Logs class
 * Created by vvdn on 23/6/18.
 */

private val isLogEnabled: Boolean = true

public val BASE_URL:String = "http://127.0.0.1:8080/" // 127.0.0.1 my system location host
public val API_URL :String ="http://10.0.2.2:8080/explore"
fun v(logTag: String, logMessageToPrint: String) {
    if (isLogEnabled)
        Log.v(logTag, logMessageToPrint)
}