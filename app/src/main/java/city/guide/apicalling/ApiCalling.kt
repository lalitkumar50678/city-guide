package city.guide.apicalling

import city.guide.interfaces.ApiInterface
import city.guide.interfaces.ResponseInterface
import city.guide.utils.API_URL
import city.guide.utils.BASE_URL
import city.guide.utils.v
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.IOException

class ApiCalling{

    val TAG: String  = "ApiCalling"

    fun getLocation(s: String, responseInterface: ResponseInterface) {
        val apiService = getClient()!!.create(ApiInterface::class.java)
        val call: Call<ResponseBody>

        call = apiService.getLatlongs(s)
        v(TAG, "GET google api $s")

        call.enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                try {
                    if (response != null && response.body() != null) {
                        val responseC = response.body()!!.string()
                        v(TAG, "response $responseC")
                        responseInterface.onResponseListener(responseC, API_URL)
                    } else {
                        responseInterface.onFailListener("")
                    }
                } catch (e: IOException) {
                    e.printStackTrace()
                }

            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                t.printStackTrace()
                responseInterface.onFailListener("")
            }
        })
    }

}