package city.guide.apicalling

import android.util.Log
import city.guide.utils.BASE_URL
import okhttp3.*
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.io.File
import java.util.concurrent.TimeUnit

class ApiClient{
    private val TAG = ApiClient::class.java.simpleName

    private val TEMP_FILE_NAME = "GaraG"

    companion object {


    }



    fun createBuilder(paramsName: Array<String>, paramsValue: Array<String>): FormBody.Builder {
        val builder = FormBody.Builder()

        for (i in paramsName.indices) {
            Log.e(TAG, "createBuilder: Name  " + paramsName[i] + " value " + paramsValue[i])
            builder.add(paramsName[i], paramsValue[i])
        }
        return builder
    }


}
private  var retrofit: Retrofit? = null

fun getClient(): Retrofit? {
    if (retrofit == null) {
        val okHttpClient = OkHttpClient().newBuilder()
                .connectTimeout(80, TimeUnit.SECONDS)
                .readTimeout(80, TimeUnit.SECONDS)
                .writeTimeout(80, TimeUnit.SECONDS)
                .build()

        retrofit = Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
    }
    return retrofit
}