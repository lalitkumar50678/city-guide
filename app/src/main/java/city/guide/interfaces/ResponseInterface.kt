package city.guide.interfaces

interface ResponseInterface {
    fun onResponseListener(response: String, apiName: String)
    fun onFailListener(response: String)
}